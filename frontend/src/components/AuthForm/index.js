import React, { useState } from 'react';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

import authService from '../../services/authService';

export default ({ history, registerForm }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');

  const onUsernameChange = (e) => {
    setUsername(e.target.value);
  }

  const onPasswordChange = (e) => {
    setPassword(e.target.value);
  }

  const onEmailChange = (e) => {
    setEmail(e.target.value);
  }

  const onSubmit = () => {
    if (registerForm) {
      authService.register(username, password, email, () => history.push('/login'));
    } else {
      authService.login(username, password, () => history.push('/films'));
    }
  }

  return (
    <Form>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Username</Form.Label>
        <Form.Control type="username" placeholder="Enter username" value={username} onChange={onUsernameChange} />
      </Form.Group>

      {registerForm && (
         <Form.Group controlId="formBasicEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={email} onChange={onEmailChange} />
        </Form.Group>
      )}

      <Form.Group controlId="formBasicPassword">
        <Form.Label>Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password} onChange={onPasswordChange} />
      </Form.Group>

      <Button variant="primary" onClick={onSubmit}>
        {registerForm ? 'Register' : 'Login'}
      </Button>
    </Form>
  )
}
