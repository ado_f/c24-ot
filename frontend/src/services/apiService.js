const BASE_API_URL = 'http://localhost:8081';

const post = (url, data) => {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return fetch(`${BASE_API_URL}/${url}`, {
    method: 'post',
    body: JSON.stringify(data),
    headers,
  }).then(response => response.json())
    .catch(e => console.warn('error', e))
}

export default {
  post
}