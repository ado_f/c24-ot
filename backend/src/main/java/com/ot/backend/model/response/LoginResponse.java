package com.ot.backend.model.response;

public class LoginResponse extends ApiResponse {
    private String token;

    public LoginResponse(Boolean success, String message, String token) {
        super(success, message);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
