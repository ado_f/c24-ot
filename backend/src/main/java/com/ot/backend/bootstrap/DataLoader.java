package com.ot.backend.bootstrap;

import com.ot.backend.model.Film;
import com.ot.backend.model.Ranking;
import com.ot.backend.model.User;
import com.ot.backend.repository.FilmRepository;
import com.ot.backend.repository.UserRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;

@Component
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {
    private UserRepository userRepository;

    private FilmRepository filmRepository;

    private PasswordEncoder passwordEncoder;

    public DataLoader(UserRepository userRepository, PasswordEncoder passwordEncoder, FilmRepository filmRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.filmRepository = filmRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        User user = new User("test", passwordEncoder.encode("test"), "john@doe.com");
        userRepository.save(user);

        Film film1 = new Film("film1", "random description", new ArrayList<>());
        filmRepository.save(film1);

        Film film2 = new Film("film1", "random description", new ArrayList<>());
        filmRepository.save(film2);
    }
}
