package com.ot.backend.controller;

import com.ot.backend.model.request.AuthRequest;
import com.ot.backend.model.request.RegisterRequest;
import com.ot.backend.model.response.ApiResponse;
import com.ot.backend.model.response.LoginResponse;
import com.ot.backend.service.UserService;
import com.ot.backend.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.Response;

@RestController
public class AuthController {
    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword())
            );
        } catch (Exception e) {
            throw new Exception("Invalid username/password");
        }

        LoginResponse response = new LoginResponse(
                true,
                "Successfully logged in",
                jwtUtil.generateToken(authRequest.getUsername())
        );

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity registerUser(@RequestBody final RegisterRequest registerRequest, final HttpServletRequest request) {
        if (userService.existsByEmail(registerRequest.getEmail())) {
            return new ResponseEntity<>(new ApiResponse(false, "Email is already taken!"),
                    HttpStatus.OK);
        }

        if(userService.existsByUserName(registerRequest.getUserName())){
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.OK);
        }

        return userService.registerUser(registerRequest);
    }
}
