package com.ot.backend.model;

import javax.persistence.*;

@Entity
@Table(name = "RANKING")
public class Ranking extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false)
    private Film film;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
}
