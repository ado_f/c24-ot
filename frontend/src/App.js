import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from "react-router-dom";

import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'

import Home from './pages/Home';
import FilmsPage from './pages/Films';
import LoginPage from './pages/Login';
import RegisterPage from './pages/Register';
import LoginButton from './components/LoginButton';

import authService from './services/authService';

function App() {
  return (
    <Router>
      <Navbar expand="lg" bg="dark" variant="dark">
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Link to="/home" className="pl-3">Home</Link>
            <Link to="/films" className="pl-3">Films</Link>
          </Nav>
        </Navbar.Collapse>
        <Nav>
          <LoginButton/>
        </Nav>
      </Navbar>

      <Container className="p-5">
        <Route path="/home" component={Home}/>
        <Route path="/films" component={FilmsPage}/>
        
        <Route path="/login" component={LoginPage}/>
        <Route path="/register" component={RegisterPage}/>
      </Container>
    </Router>
  );
}

export default App;
