package com.ot.backend.repository;

import com.ot.backend.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    User findByUserName(String userName);
    Boolean existsByEmail(final String email);
    Boolean existsByUserName(final String username);
}
