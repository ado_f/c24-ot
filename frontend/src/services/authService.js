import apiService from './apiService';

const login = (username, password, callback) => {
  apiService.post('login', { username, password })
    .then(res => {
      if (!res.success) {
        alert('Login failed. Check credentials!');
      } else {
        localStorage.setItem('authToken', res.token);
        callback();
      }
    });
};

const register = (username, password, email, callback) => {
  apiService.post('register', { userName: username, password, email })
    .then(() => callback());
}

const logout = (callback) => {
  localStorage.removeItem('authToken');
  callback();
};

const isLoggedIn = () => {
  return !!localStorage.getItem('authToken');
};

export default {
  login,
  isLoggedIn,
  logout,
  register
}
