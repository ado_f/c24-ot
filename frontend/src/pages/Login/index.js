import React from 'react';

import AuthForm from '../../components/AuthForm';

export default ({ history }) => {
  return <AuthForm history={history} registerForm={false} />;
}
