import React from 'react';
import { withRouter } from "react-router";
import {
  Link
} from "react-router-dom";
import authService from '../../services/authService';

const AuthButton = withRouter(({ history }) => (
  authService.isLoggedIn() ? (
      <button onClick={() => {
        authService.logout(() => history.push('/'))
      }}>Log out</button>
  ) : (
    !authService.isLoggedIn() && 
      <>
        <Link to="/login" className="pl-3">Login</Link>
        <Link to="/register" className="pl-3">Register</Link>
      </>
  )
));

export default AuthButton;