package com.ot.backend.service;

import com.ot.backend.model.User;
import com.ot.backend.model.request.RegisterRequest;
import com.ot.backend.model.response.ApiResponse;
import com.ot.backend.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public boolean existsByEmail(final String email) {
        return userRepository.existsByEmail(email);
    }

    public boolean existsByUserName(final String username) {
        return userRepository.existsByUserName(username);
    }

    public ResponseEntity<ApiResponse> registerUser(RegisterRequest registerRequest) {

        try {
            User newUser = userRepository.save(
                new User(
                    registerRequest.getUserName(),
                    passwordEncoder.encode(registerRequest.getPassword()),
                    registerRequest.getEmail()
                )
            );
            return new ResponseEntity<>(new ApiResponse(
                    true,
                    String.format("User %s created", newUser.getEmail())),
                    HttpStatus.OK
            );
        } catch (Exception e) {
            return new ResponseEntity<>(new ApiResponse(false, "Bad request data!"),
                    HttpStatus.BAD_REQUEST);
        }
    }
}
