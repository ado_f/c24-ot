package com.ot.backend.model;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "FILM")
public class Film extends BaseModel {
    @NotNull
    private String name;
    @NotNull
    private String description;

    @OneToMany(mappedBy="film")
    private List<Ranking> rankings = new ArrayList<>();

    public Film() {
    }

    public Film(@NotNull String name, @NotNull String description, List<Ranking> rankings) {
        this.name = name;
        this.description = description;
        this.rankings = rankings;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Ranking> getRankings() {
        return rankings;
    }

    public void setRankings(List<Ranking> rankings) {
        this.rankings = rankings;
    }
}
